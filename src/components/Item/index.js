import React from 'react'
import styles from './Item.module.scss'
import { mudarFavorito } from 'store/reducers/itens'
import { mudarCarrinho, mudarQuantidade } from 'store/reducers/carrinho'
import {
  AiOutlineHeart,
  AiFillHeart,
  AiFillMinusCircle,
  AiFillPlusCircle
} from 'react-icons/ai'
import {
  FaCartPlus
} from 'react-icons/fa'
import { useDispatch, useSelector } from 'react-redux'
import classNames from 'classnames'

const iconProps = {
  size:24,
  color:'#041833'
}

const quantidadeProps = {
  size:32,
  color:'#1875E8'
}

function Item({titulo, foto, id, preco, descricao, favorito, carrinho, quantidade}) {

  const dispatch = useDispatch()
  const estaNoCarrinho = useSelector(state => 
    state.carrinho.some(itemNoCarrinho => 
      itemNoCarrinho.id === id))

  function resolverFavorito(){
    dispatch(mudarFavorito(id))
  }

  function resolverCarrinho(){
    dispatch(mudarCarrinho(id))
  }

  return (
    <div className={classNames(styles.item, {
      [styles.itemNoCarrinho]: carrinho
    })} key={id}>
      <div className={styles['item-imagem']}>
        <img src={foto} alt={titulo} />
      </div>
      <div className={styles['item-descricao']}>
        <div className={styles['item-titulo']}>
          <h2>
            {titulo}
          </h2>
          <p>
            {descricao}
          </p>
        </div>
        <div className={styles['item-info']}>
          <div className={styles['item-preco']}>
            R$ {preco.toFixed(2)}
          </div>
          <div className={styles['item-acoes']}>
            {!favorito ? 
              <AiOutlineHeart {...iconProps} 
                onClick={() => resolverFavorito()} 
                className={styles['item-acao']} 
              /> 
              : 
              <AiFillHeart {...iconProps} 
                onClick={() => resolverFavorito()} 
                color={'#ff0000'} 
                className={styles['item-acao']} 
              />
            }
            {
              carrinho ? 
              (
                <div className={styles.quantidade}>
                  Quantidade: 
                  <AiFillMinusCircle {...quantidadeProps}
                    onClick={() =>  {
                      if(quantidade >= 1)
                        dispatch(mudarQuantidade({ id: id, quantidade: -1}))
                    }} 
                  />
                  <span>{String(quantidade || 0).padStart(2,'0')}</span>
                  <AiFillPlusCircle {...quantidadeProps} 
                    onClick={() =>  dispatch(mudarQuantidade({ id: id, quantidade: 1}))} 
                  />
                </div>
              )
              :
              (
                <FaCartPlus {...iconProps} 
                  color={estaNoCarrinho ? '#1875E8' : iconProps.color} 
                  className={styles['item-acao']}
                  onClick={() => resolverCarrinho()} 
                />
              )
            }
            
          </div>
        </div>
      </div>
    </div>
  )
}

export default Item